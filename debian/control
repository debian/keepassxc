Source: keepassxc
Section: utils
Priority: optional
Maintainer: Julian Andres Klode <jak@debian.org>
Build-Depends: asciidoctor,
               cmake,
               dbus-daemon <!nocheck> | dbus <!nocheck>,
               debhelper-compat (= 13),
               libargon2-dev | libargon2-0-dev,
               libbotan-2-dev,
               libcurl4-gnutls-dev,
               libminizip-dev,
               libpcsclite-dev,
               libqrencode-dev,
               libqt5svg5-dev,
               libqt5x11extras5-dev,
               libreadline-dev,
               libusb-1.0-0-dev,
               libxtst-dev,
               libzxcvbn-dev,
               qtbase5-dev,
               qtbase5-private-dev,
               qttools5-dev,
               qttools5-dev-tools,
               xauth <!nocheck>,
               xclip <!nocheck>,
               xvfb <!nocheck>,
               zlib1g-dev
Standards-Version: 4.6.2
Homepage: https://keepassxc.org/
Vcs-Git: https://salsa.debian.org/debian/keepassxc.git
Vcs-Browser: https://salsa.debian.org/debian/keepassxc

Package: keepassxc
Architecture: all
Section: oldlibs
Depends: ${misc:Depends}, keepassxc-full | keepassxc-minimal
Description: Cross Platform Password Manager (transitional package)
 KeePassXC is a free/open-source password manager or safe which helps you
 to manage your passwords in a secure way. You can put all your
 passwords in one database, which is locked with one master key or a
 key-disk. So you only have to remember one single master password or
 insert the key-disk to unlock the whole database. The databases are
 encrypted using the algorithms AES or Twofish.
 .
 In contrast to KeePassX (package keepassx), KeePassXC is actively developed
 and has more features, e.g., connectivity to a Web Browser plugin (package
 webext-keepassxc-browser).
 .
 This is a transitional package depending on keepassxc-full or keepassxc-minimal

Package: keepassxc-minimal
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: keepassxc (<< 2.7.7+dfsg.1-3~)
Conflicts: keepassxc-full
Replaces: keepassxc
Provides: keepassxc (= ${binary:Version})
Recommends: fonts-font-awesome
Description: Cross Platform Password Manager
 KeePassXC is a free/open-source password manager or safe which helps you
 to manage your passwords in a secure way. You can put all your
 passwords in one database, which is locked with one master key or a
 key-disk. So you only have to remember one single master password or
 insert the key-disk to unlock the whole database. The databases are
 encrypted using the algorithms AES or Twofish.
 .
 In contrast to KeePassX (package keepassx), KeePassXC is actively developed
 and has more features, e.g., connectivity to a Web Browser plugin (package
 webext-keepassxc-browser).
 .
 This package includes only the bare minimal functionality, and no security
 complications like networking, SSH agent, browser plugin, fdo secret storage.
 See keepassxc-full if you absolutely need those.

Package: keepassxc-full
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: keepassxc (<< 2.7.7+dfsg.1-3~)
Conflicts: keepassxc-minimal
Replaces: keepassxc
Provides: keepassxc (= ${binary:Version})
Recommends: fonts-font-awesome
Suggests: webext-keepassxc-browser, xclip
Description: Cross Platform Password Manager
 KeePassXC is a free/open-source password manager or safe which helps you
 to manage your passwords in a secure way. You can put all your
 passwords in one database, which is locked with one master key or a
 key-disk. So you only have to remember one single master password or
 insert the key-disk to unlock the whole database. The databases are
 encrypted using the algorithms AES or Twofish.
 .
 In contrast to KeePassX (package keepassx), KeePassXC is actively developed
 and has more features, e.g., connectivity to a Web Browser plugin (package
 webext-keepassxc-browser).
 .
 This package includes all plugins, including networking, and various IPC
 like browser integration, ssh agent, freedesktop.org secret storage. Use
 at your own risk.
